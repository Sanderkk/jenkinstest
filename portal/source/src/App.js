import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>Bazefield Jenkins test code</h1>
    </div>
  );
}

export default App;
