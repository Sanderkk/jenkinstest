# A repository used for testing CI & CD in Jenkins


## Docker
This is all run inside of a docker container.
The jenkins container is based in the ```jenkins/jenkins:lts``` image.

### Creating the docker image
To create the jenkins pipeline, we create a container from the image
```jenkins/jenkins:lts```
This can be run with several different flags, but is is usefull to use the flag
``` --priviliged ```
so that the container runs in privileged mode, and
``` -p ```
to expose the port 8080 to access the Jenkins GUI.
Other perfered flags are:
``` --name ```
to set a name for the container, making it easier to locate. And the
``` -v ```
to share a file location to access the files of the docker container, as this might also be usefull.
#### Example run
An example of this run can then be:
``` docker run --name Baze_Jenkins_CICD --privileged -p 8080:8080 -p 50000:50000 -v /c/Repos/CICD/BazeDocker:/var/jenkins_home jenkins/jenkins:lts ```

Alt
``` docker run --name Jenkins_Baze --privileged -p 8080:8080 -p 50000:50000 -v /c/Repos/CICD/Jenkins_Baze:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v ${which docker}:/usr/bin/docker jenkins/docker ```

### Getting started
After the docker container is started, given that port 8080 is exposed, the Jenkins GUI can be accessed.
In the docker files, as found in the shared file system path, the secret folder contains a file with the initial admin password for the Jenkins GUI. The file is named "initialAdminPassword".
After this, just follow the on screen instructions.
The easiest thing would be to installed the advised plugins, as there are a lot of usefull packages here we need, like git and pipeline, but all of these can be installed at a later point so there is no need to stress about this.

### Upp and running
Once the jenkins instance is configured and the initial setup is finished, given that git and pipeline plugins are installed, we create a new jobb, creating a new pipeline.

#### Installing additional plugins
Additional packages needed are:
``` Docker ```
``` Docker pipeline ```

#### Creating the pipeline
In the pipeline settings, choose the pipeline deffinition "Pipeline script from SCM".
Choose SCM as "Git".
Here we can deffine the repository URL, and the needed credentials for the git repository. The branches one wisshes to build can also be spessified here.

It is also possible to deffine the script path for the repository jenkins file.


docker run -u root -d -p 8080:8080 -v /c/Repos/CICD/JenkinsBaze:/var/jenkins_home -v npm-cache:/root/.npm -v cypress-cache:/root/.cache -v /var/run/docker.sock:/var/run/docker.sock --name JenkinsBaze jenkinsci/blueocean:latest